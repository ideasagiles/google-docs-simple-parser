/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

var urlJson = "http://spreadsheets.google.com/feeds/cells/0ApvTNPAOSjfTdHRFeEJRZzhlZnNjQnUwMjBqNmh6QVE/1/public/basic?alt=json";
var urlTxt = "https://docs.google.com/spreadsheet/pub?key=0ApvTNPAOSjfTdHRFeEJRZzhlZnNjQnUwMjBqNmh6QVE&single=true&gid=0&output=txt";

module("parseSpreadsheetTextUrl");

asyncTest("parseSpreadsheetTextUrl_validUrl_returnsObjectArray", function () {
    expect(37);
    googleDocsSimpleParser.parseSpreadsheetTxtUrl({
        url: urlTxt,
        done: function (people) {
            testPeople(people);
            start();
        }
    });
});

asyncTest("parseSpreadsheetTextUrl_withNameToUppercaseTestTransformer_returnsObjectArrayWithNamesInUppercase", function () {
    expect(49);
    googleDocsSimpleParser.parseSpreadsheetTxtUrl({
        url: urlTxt,
        transformer: nameToUppercaseTestTransformer,
        done: function (people) {
            testPeople(people, {testNameInUppercase: true});
            start();
        }
    });
});

module("parseSpreadsheetCellsUrl");

asyncTest("parseSpreadsheetCellsUrl_validUrl_returnsObjectArray", function () {
    expect(37);
    googleDocsSimpleParser.parseSpreadsheetCellsUrl({
        url: urlJson,
        done: function (people) {
            testPeople(people);
            start();
        }
    });
})

asyncTest("parseSpreadsheetCellsUrl_withNameToUppercaseTestTransformer_returnsObjectArrayWithNamesInUppercase", function () {
    expect(49);
    googleDocsSimpleParser.parseSpreadsheetCellsUrl({
        url: urlJson,
        transformer: nameToUppercaseTestTransformer,
        done: function (people) {
            testPeople(people, {testNameInUppercase: true});
            start();
        }
    });
})


/*****************************************************************
 * Test utility functions.
 *****************************************************************/

/** Checks an array of people. */
function testPeople(people, settings) {
    var i, expectedId;
    settings = settings || {};

    ok(people.length > 0, "People found");

    for (i = 0; i < people.length; i++) {
        assertPersonHasAllAttributes(people[i]);
        expectedId = i + 1;
        equal(people[i].id, expectedId, "Id attribute has expected value.");

        if (settings.testNameInUppercase) {
            equal(people[i].name, people[i].name.toUpperCase(), "Name is in uppercase");
            equal(people[i].lastName, people[i].lastName.toUpperCase(), "Name is in uppercase");
        }
    }
}


/** Checks that a person has all attriutes setted and not empty. */
function assertPersonHasAllAttributes(person) {
    ok(person.id && person.id.length > 0, "Id attribute is ok.");
    ok(person.name && person.name.length > 0, "Name attribute is ok.");
    ok(person.lastName && person.lastName.length > 0, "Last name attribute is ok.");
    ok(person.lastLogin && person.lastLogin.length > 0, "Last login attribute is ok.");
    ok(person.description && person.description.length > 0, "Description attribute is ok.");
}

function nameToUppercaseTestTransformer(person) {
    person.name = person.name.toUpperCase();
    person.lastName = person.lastName.toUpperCase();
    return person;
}
